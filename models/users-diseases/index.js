const sequelize = require('sequelize');

const db = require('../../config/db');

const usersDiseases = db.define('UsersDiseases', {
  Id: {
    type: sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  UserId: {
    type: sequelize.INTEGER,
    allowNull: false,
  },
  DiseaseCode: {
    type: sequelize.STRING(31),
    allowNull: false,
  },
}, {
  timestamps: true,
});

const establishRelations = models => {
  const { users, diseases, usersDiseases } = models;
  
  users.belongsToMany(diseases, {
    through: usersDiseases,
    as: usersDiseases.aliasPlural,
  });
  
  diseases.belongsToMany(users, {
    through: usersDiseases,
    as: usersDiseases.aliasPlural,
  });
};

module.exports = {
  alias: 'UsersDisease',
  aliasPlural: 'UsersDiseases',
  model: usersDiseases,
  establishRelations,
};
