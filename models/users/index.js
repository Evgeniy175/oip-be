const sequelize = require('sequelize');

const db = require('../../config/db');

const users = db.define('Users', {
  Id: {
    type: sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  FirstName: {
    type: sequelize.STRING(31),
    allowNull: false,
  },
  LastName: {
    type: sequelize.STRING(31),
    allowNull: false,
  },
}, {
  timestamps: true,
});

const establishRelations = models => {};

module.exports = {
  alias: 'User',
  aliasPlural: 'Users',
  model: users,
  establishRelations,
};
