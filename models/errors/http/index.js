const { DEFAULT_ERROR } = require('../../../services/errors');

class HttpError extends Error {
  constructor(options = {}) {
    super();
    Object.assign(this, DEFAULT_ERROR, options);
    Error.captureStackTrace(this, HttpError);
  }
}

module.exports = HttpError;
