const sequelize = require('sequelize');

const db = require('../../config/db');

const diseases = db.define('Diseases', {
  Code: {
    type: sequelize.STRING(31),
    primaryKey: true,
  },
  Name: {
    type: sequelize.STRING(255),
    allowNull: false,
  },
}, {
  timestamps: true,
});

const establishRelations = models => {};

module.exports = {
  alias: 'Disease',
  aliasPlural: 'Diseases',
  model: diseases,
  establishRelations,
};
