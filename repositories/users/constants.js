const Database = require('../../services/db');

const FETCH_INCLUDE = [
  {
    model: Database.models.diseases,
  },
];

module.exports = {
  FETCH_INCLUDE,
};
