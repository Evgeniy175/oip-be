const Database = require('../../services/db');

const BaseRepository = require('../base');

const { FETCH_INCLUDE } = require('./constants');

class UsersRepository extends BaseRepository {
  constructor() {
    super(Database.models.users);
  }
  
  getUserById(id, options) {
    const resultOptions = this.mergeOptions({
      include: FETCH_INCLUDE,
    }, options);
    return super.findById(id, resultOptions);
  }
  
  getUsers(options) {
    const resultOptions = this.mergeOptions({
      include: FETCH_INCLUDE,
    }, options);
    return super.findAll(resultOptions);
  }
  
  updateById(entity, options) {
    const resultOptions = this.mergeOptions({
      where: {
        Id: entity.Id,
      },
    }, options);
    return super.update(entity, resultOptions);
  }
  
  remove(id, options) {
    const resultOptions = this.mergeOptions({
      where: {
        Id: id,
      },
    }, options);
    return super.destroy(resultOptions);
  }
}

module.exports = UsersRepository;
