const deepAssign = require('deep-assign');

const { handleSequelizeError } = require('../../services/db/errors');

class BaseRepository {
  constructor(model) {
    this.model = model;
  }

  async create(entity, options = {}) {
    try {
      return await this.model.create(entity, options);
    } catch (e) {
      handleSequelizeError(e);
    }
  }

  async findOne(options) {
    try {
      return await this.model.findOne(options);
    } catch (e) {
      handleSequelizeError(e);
    }
  }

  async findById(id, options) {
    try {
      return await this.model.findByPk(id, options);
    } catch (e) {
      handleSequelizeError(e);
    }
  }

  async findAll(options) {
    try {
      return await this.model.findAll(options);
    } catch (e) {
      handleSequelizeError(e);
    }
  }

  async upsert(entity, options) {
    try {
      const item = await this.model.find(options);
      return await item ? this.update(entity, options) : this.create(entity);
    } catch (e) {
      handleSequelizeError(e);
    }
  }

  async update(entity, options) {
    try {
      return await this.model.update(entity, options);
    } catch (e) {
      handleSequelizeError(e);
    }
  }

  async destroy(options) {
    try {
      return await this.model.destroy(options);
    } catch (e) {
      handleSequelizeError(e);
    }
  }

  async max(fieldName, options) {
    try {
      return await this.model.max(fieldName, options);
    } catch (e) {
      handleSequelizeError(e);
    }
  }

  mergeOptions(initial, additional) {
    return deepAssign(initial, additional);
  }
}

module.exports = BaseRepository;
