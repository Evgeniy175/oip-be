const Database = require('../../services/db');

const BaseRepository = require('../base');

class DiseasesRepository extends BaseRepository {
  constructor() {
    super(Database.models.diseases);
  }
  
  updateByCode(entity, options) {
    const resultOptions = this.mergeOptions({
      where: {
        Code: entity.Code,
      },
    }, options);
    return super.update(entity, resultOptions);
  }
  
  removeByCode(code, options) {
    const resultOptions = this.mergeOptions({
      where: {
        Code: code,
      },
    }, options);
    return super.destroy(resultOptions);
  }
}

module.exports = DiseasesRepository;
