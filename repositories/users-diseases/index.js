const Database = require('../../services/db');

const BaseRepository = require('../base');

class UsersDiseasesRepository extends BaseRepository {
  constructor() {
    super(Database.models.usersDiseases);
  }
}

module.exports = UsersDiseasesRepository;
