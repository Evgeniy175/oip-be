const express = require('express');
const http = require('http');
const cors = require('cors');

const { db } = require('./config');

const cacheService = require('./services/cache');

const app = express();
const port = process.env.PORT || 3001;

const Database = require('./services/db'); // Do not remove! Needed to establish DB relations.
const DatabaseInitializer = require('./services/db/initializer');

const initApi = require('./controllers');

const server = http.createServer(app);

(async () => {
  await Database.sync({ force: db.force });
  await DatabaseInitializer(Database);
  await cacheService.init();
  
  app.use(cors());
  app.use(express.json());
  initApi({ app });

  server.listen(port, () => console.log(`API running on http://${server.address().address}:${port}`));
})();
