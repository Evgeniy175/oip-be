const { HttpError } = require('../../../services/errors');

module.exports = (err, req, res, next) => {
  if (!err) return next();
  
  console.error(err);
  
  res.status(err.status || 500);
  return err.message && err instanceof HttpError ? res.send({ message: err.message }) : res.end();
};
