const express = require('express');

const router = express.Router();

const { errorMw } = require('./mw');

module.exports = ({ app }) => {
  require('./users')(router);
  require('./diseases')(router);
  
  app.use('/', router);
  
  app.use(errorMw);
};
