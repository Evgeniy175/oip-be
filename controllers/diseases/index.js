const express = require('express');

const DiseasesService = require('../../services/db/diseases');

const router = express.Router();

const diseasesService = new DiseasesService();

const { ERRORS } = require('../../services/db/diseases/constants');

module.exports = rootRouter => {
  router.get('/:code', async (req, res, next) => {
    const { code } = req.params;
    
    try {
      const disease = await diseasesService.findById(code);
      res.json(disease);
    } catch(e) {
      next(e);
    }
  });
  
  router.get('/', async (req, res, next) => {
    try {
      const diseases = await diseasesService.findAll();
      res.json(diseases);
    } catch(e) {
      next(e);
    }
  });
  
  router.post('/', async (req, res, next) => {
    try {
      await diseasesService.create(req.body);
      res.status(201).end();
    } catch(e) {
      next(e);
    }
  });
  
  router.put('/:code', async (req, res, next) => {
    const { code } = req.params;
    const disease = req.body;
  
    try {
    
      if (code !== disease.Code) throw ERRORS.INVALID_ID();
    
      await diseasesService.updateByCode(disease);
      res.end();
    } catch(e) {
      next(e);
    }
  });
  
  router.delete('/:code', async (req, res, next) => {
    const { code } = req.params;
    
    try {
      await diseasesService.removeByCode(code);
      res.status(201).end();
    } catch(e) {
      next(e);
    }
  });
  
  rootRouter.use('/diseases', router);
};
