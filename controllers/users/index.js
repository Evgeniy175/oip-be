const express = require('express');

const UsersService = require('../../services/db/users');

const router = express.Router();

const usersService = new UsersService();

const { ERRORS } = require('../../services/db/users/constants');

module.exports = rootRouter => {
  router.get('/:id', async (req, res, next) => {
    const { id } = req.params;
    
    try {
      const user = await usersService.getUserById(id);
      
      if (!user) throw ERRORS.USER_NOT_FOUND();
      
      res.json(user);
    } catch(e) {
      next(e);
    }
  });
  
  router.get('/', async (req, res, next) => {
    try {
      const users = await usersService.getUsers();
      res.json(users);
    } catch(e) {
      next(e);
    }
  });
  
  router.post('/:id/diseases', async (req, res, next) => {
    const { id } = req.params;
    const disease = req.body;
    
    try {
      await usersService.addDisease({ userId: id, diseaseCode: disease.DiseaseCode });
      res.status(201).end();
    } catch(e) {
      next(e);
    }
  });
  
  router.post('/', async (req, res, next) => {
    const user = req.body;
    
    try {
      await usersService.create(user);
      res.status(201).end();
    } catch(e) {
      next(e);
    }
  });
  
  router.put('/:id', async (req, res, next) => {
    const user = req.body;
    
    try {
      const id = parseInt(req.params.id);
      
      if (id !== user.Id) throw ERRORS.INVALID_ID();
      
      await usersService.updateById(user);
      res.end();
    } catch(e) {
      next(e);
    }
  });
  
  router.delete('/:id', async (req, res, next) => {
    const { id } = req.params;
    
    try {
      await usersService.remove(id);
      res.end();
    } catch(e) {
      next(e);
    }
  });
  
  router.delete('/:userId/diseases/:diseaseCode', async (req, res, next) => {
    const { userId, diseaseCode } = req.params;
    
    try {
      await usersService.removeDisease({ userId, diseaseCode });
      res.status(201).end();
    } catch(e) {
      next(e);
    }
  });
  
  rootRouter.use('/users', router);
};
