const Sequelize = require('sequelize');
const { name, username, password, host, port, dialect, logging }  = require('./index').db;

const connection = new Sequelize(name, username, password, {
  host,
  port,
  dialect,
  logging,
  pool: {
    min: 0,
    max: 5,
    idle: 10000,
    acquire: 30000
  },
});

module.exports = connection;
