const HttpError = require('../../models/errors/http');

const ERRORS_KEYS = {
  BAD_REQUEST: 'bad request',
  UNAUTHORIZED: 'unauthorized',
  NOT_FOUND: 'not found',
  CONFLICT: 'conflict',
  SERVER: 'server',
  UNKNOWN: 'unknown',
};

const DEFAULTS_OPTIONS = {
  [ERRORS_KEYS.BAD_REQUEST]: {
    key: ERRORS_KEYS.BAD_REQUEST,
    message: 'Bad request.',
    status: 400,
  },
  [ERRORS_KEYS.UNAUTHORIZED]: {
    key: ERRORS_KEYS.UNAUTHORIZED,
    message: 'Unauthorized.',
    status: 401,
  },
  [ERRORS_KEYS.NOT_FOUND]: {
    key: ERRORS_KEYS.NOT_FOUND,
    message: 'Not found.',
    status: 404,
  },
  [ERRORS_KEYS.CONFLICT]: {
    key: ERRORS_KEYS.CONFLICT,
    message: 'Conflict.',
    status: 409,
  },
  [ERRORS_KEYS.SERVER]: {
    key: ERRORS_KEYS.SERVER,
    message: 'Server error.',
    status: 500,
  },
  [ERRORS_KEYS.UNKNOWN]: {
    key: ERRORS_KEYS.UNKNOWN,
    message: 'Unknown.',
    status: 500,
  },
};

const ERRORS = {
  [ERRORS_KEYS.BAD_REQUEST]: options => getError(ERRORS_KEYS.BAD_REQUEST, options),
  [ERRORS_KEYS.NOT_FOUND]: options => getError(ERRORS_KEYS.NOT_FOUND, options),
  [ERRORS_KEYS.CONFLICT]: options => getError(ERRORS_KEYS.CONFLICT, options),
  [ERRORS_KEYS.SERVER]: options => getError(ERRORS_KEYS.SERVER, options),
  [ERRORS_KEYS.UNKNOWN]: options => getError(ERRORS_KEYS.UNKNOWN, options),
};

const getError = (key, options = {}) => {
  return new HttpError(Object.assign({}, DEFAULTS_OPTIONS[key], options));
};

module.exports = {
  HttpError,
  ERRORS,
  ERRORS_KEYS,
  DEFAULT_ERROR: DEFAULTS_OPTIONS[ERRORS_KEYS.SERVER],
};
