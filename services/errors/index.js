const { HttpError, ERRORS_KEYS, ERRORS, DEFAULT_ERROR } = require('./constants');

class ErrorsService {
  constructor() {}

  throw(key, options = {}) {
    throw this.getError(key, options);
  }

  getError(key, options = {}) {
    return key && ERRORS[key] ? ERRORS[key](options) : ERRORS[ERRORS_KEYS.UNKNOWN](options);
  }
}

module.exports = {
  ErrorsService,
  HttpError,
  ERRORS_KEYS,
  DEFAULT_ERROR,
};
