const connection = require('../../config/db');

const Users = require('../../models/users');
const Diseases = require('../../models/diseases');
const UsersDiseases = require('../../models/users-diseases');

const { handleSequelizeError } = require('./errors');

class Database {
  constructor() {
    this.schemes = {
      users: Users,
      diseases: Diseases,
      usersDiseases: UsersDiseases,
    };

    this.schemesKeys = Object.keys(this.schemes);

    this.models = this.schemesKeys.reduce((models, key) => {
      models[key] = this.schemes[key].model;
      return models;
    }, {});

    this.establishRelations();
  }

  establishRelations() {
    this.schemesKeys.forEach(key => {
     if (!this.schemes[key].establishRelations) return;
     this.schemes[key].establishRelations(this.models);
    });
  }
  
  sync(options) {
    return connection.sync(options);
  }

  startTransaction() {
    return connection.transaction();
  }

  async rollbackTransaction(transaction, err) {
    try {
      await transaction.rollback();
    } catch (rollbackError) {
      handleSequelizeError(err || rollbackError);
    }
  }

  async query(callStatement, options) {
    try {
      return await connection.query(callStatement, options);
    } catch (e) {
      handleSequelizeError(e);
    }
  }
}

module.exports = new Database();
