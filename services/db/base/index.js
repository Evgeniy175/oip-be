class BaseService {
  constructor(repository) {
    this.repository = repository;
  }

  create(entity, options) {
    return this.repository.create(entity, options);
  }

  findOne(options) {
    return this.repository.findOne(options);
  }

  findById(id, options) {
    return this.repository.findById(id, options);
  }

  findAll(options) {
    return this.repository.findAll(options);
  }

  upsert(entity, options) {
    return this.repository.upsert(entity, options);
  }

  update(entity, options) {
    return this.repository.update(entity, options);
  }

  destroy(options) {
    return this.repository.destroy(options);
  }
}

module.exports = BaseService;
