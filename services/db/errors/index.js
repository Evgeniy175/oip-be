const { ERRORS } = require('./constants');

const { ErrorsService } = require('../../errors');

const errorsService = new ErrorsService();

const handleSequelizeError = e => {
  const error = ERRORS.find(error => e instanceof error.class);
  if (!error) return console.error(e);
  errorsService.throw(error.key, { sequelizeError: e });
};

module.exports = {
  handleSequelizeError,
};
