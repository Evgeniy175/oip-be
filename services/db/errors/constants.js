const Sequelize = require('sequelize');

const { ERRORS_KEYS } = require('../../errors');

const ERRORS = [
  {
    class: Sequelize.ValidationError,
    key: ERRORS_KEYS.BAD_REQUEST
  },
  {
    class: Sequelize.UniqueConstraintError,
    key: ERRORS_KEYS.BAD_REQUEST
  },
  {
    class: Sequelize.ForeignKeyConstraintError,
    key: ERRORS_KEYS.SERVER
  },
  {
    class: Sequelize.SequelizeScopeError,
    key: ERRORS_KEYS.SERVER
  },
  {
    class: Sequelize.OptimisticLockError,
    key: ERRORS_KEYS.SERVER
  },
  {
    class: Sequelize.DatabaseError,
    key: ERRORS_KEYS.SERVER
  },
  {
    class: Sequelize.TimeoutError,
    key: ERRORS_KEYS.SERVER
  },
  {
    class: Sequelize.ExclusionConstraintError,
    key: ERRORS_KEYS.SERVER
  },
  {
    class: Sequelize.UnknownConstraintError,
    key: ERRORS_KEYS.SERVER
  },
  {
    class: TypeError,
    key: ERRORS_KEYS.SERVER
  },
];

module.exports = {
  ERRORS,
};
