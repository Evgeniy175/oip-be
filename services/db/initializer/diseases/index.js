const diseases = require('./diseases');

module.exports = ({ models }) => {
  return models.diseases.bulkCreate(diseases, {
    fields: ["Code", "Name"] ,
    updateOnDuplicate: [],
  });
};
