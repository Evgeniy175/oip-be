const users = require('./users');

module.exports = ({ models }) => {
  return models.users.bulkCreate(users, {
    fields: ["Id", "FirstName", "LastName"] ,
    updateOnDuplicate: [],
  });
};
