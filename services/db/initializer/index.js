const DiseasesInitializer = require('./diseases');
const UsersInitializer = require('./users');

module.exports = async (database) => {
  return Promise.all([
    DiseasesInitializer(database),
    UsersInitializer(database),
  ]);
};
