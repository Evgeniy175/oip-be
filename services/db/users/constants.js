const { ErrorsService, ERRORS_KEYS } = require('../../errors/index');

const errorsService = new ErrorsService();

const ERRORS = {
  INVALID_ID: () => errorsService.getError(ERRORS_KEYS.BAD_REQUEST, { message: 'User ID is invalid.' }),
  
  USER_NOT_FOUND: () => errorsService.getError(ERRORS_KEYS.NOT_FOUND, { message: 'User not found.' }),
};

module.exports = {
  ERRORS,
};
