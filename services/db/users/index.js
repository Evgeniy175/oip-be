const UsersRepository = require('../../../repositories/users');
const DiseasesRepository = require('../../../repositories/diseases');

const BaseService = require('../base');

const { ERRORS } = require('./constants');
const { ERRORS: DISEASES_ERRORS } = require('../diseases/constants');

class UsersService extends BaseService {
  constructor() {
    super(new UsersRepository());
    this.diseasesRepository = new DiseasesRepository();
  }
  
  async addDisease({ userId, diseaseCode }, options) {
    const [user, disease] = await this.findUserWithDesease({ userId, diseaseCode }, options);
    
    if (!user) throw ERRORS.USER_NOT_FOUND();
    if (!disease) throw DISEASES_ERRORS.DISEASE_NOT_FOUND();
    
    return user.addDisease(disease, options);
  }
  
  getUserById(id, options) {
    return this.repository.getUserById(id, options);
  }
  
  getUsers(options) {
    return this.repository.getUsers(options);
  }
  
  updateById(entity, options) {
    return this.repository.updateById(entity, options);
  }
  
  remove(id, options) {
    return this.repository.remove(id, options);
  }
  
  async removeDisease({ userId, diseaseCode }, options) {
    const [user, disease] = await this.findUserWithDesease({ userId, diseaseCode }, options);
  
    if (!user) throw ERRORS.USER_NOT_FOUND();
    if (!disease) throw DISEASES_ERRORS.DISEASE_NOT_FOUND();
    
    return user.removeDisease(disease);
  }
  
  findUserWithDesease({ userId, diseaseCode }, options) {
    return Promise.all([
      this.getUserById(userId, options),
      this.diseasesRepository.findById(diseaseCode, options),
    ]);
  }
}

module.exports = UsersService;
