const { ErrorsService, ERRORS_KEYS } = require('../../errors/index');

const errorsService = new ErrorsService();

const ERRORS = {
  INVALID_ID: () => errorsService.getError(ERRORS_KEYS.BAD_REQUEST, { message: 'Disease ID is invalid.' }),
  
  DISEASE_NOT_FOUND: () => errorsService.getError(ERRORS_KEYS.NOT_FOUND, { message: 'Disease not found.' }),
};

module.exports = {
  ERRORS,
};
