const DiseasesRepository = require('../../../repositories/diseases');

const BaseService = require('../base');

class DiseasesService extends BaseService {
  constructor() {
    super(new DiseasesRepository());
  }
  
  updateByCode(entity, options) {
    return this.repository.updateByCode(entity, options);
  }
  
  removeByCode(code, options) {
    return this.repository.removeByCode(code, options);
  }
}

module.exports = DiseasesService;
