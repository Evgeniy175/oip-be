class CachingService {
  get providersKeys() {
    return Object.keys(this.providers);
  }

  constructor() {
    this.providers = {
    };
  }

  async init() {
    const fetchers = this.providersKeys.map(key => this.providers[key].init());
    await Promise.all(fetchers);
  }
}

module.exports = new CachingService();
